from skimage.transform import AffineTransform, warp
import numpy as np
import albumentations as A
from utils import crop_char_image, resize
import cv2


def trigger(method):
    #for debugging purpose
    # print(f"{method} was triggered")
    return


def affine_image(img):
    """

    Args:
        img: (h, w) or (1, h, w)

    Returns:
        img: (h, w)
    """
    # ch, h, w = img.shape
    # img = img / 255.
    if img.ndim == 3:
        img = img[0]

    # --- scale ---
    min_scale = 0.7
    max_scale = 1.0
    sx = np.random.uniform(min_scale, max_scale)
    sy = np.random.uniform(min_scale, max_scale)

    # --- rotation ---
    max_rot_angle = 2
    rot_angle = np.random.uniform(-max_rot_angle, max_rot_angle) * np.pi / 180.

    # --- shear ---
    max_shear_angle = 3
    shear_angle = np.random.uniform(
        -max_shear_angle, max_shear_angle) * np.pi / 180.

    # --- translation ---
    max_translation = 1
    tx = np.random.randint(-max_translation, max_translation)
    ty = np.random.randint(-max_translation, max_translation)

    r = np.random.uniform()
    if r < 1:
        tform = AffineTransform(scale=(sx, sy))
        trigger("scale")
    # elif r < 1:
    #     tform = AffineTransform(scale=(1, 1), rotation=rot_angle, shear=0, translation=(0, 0))
    #     trigger("rotation")
    # elif r < 1:
    #     tform = AffineTransform(shear=shear_angle)
    # else:
    #     tform = AffineTransform(translation=(tx, ty))

    transformed_image = warp(img, tform)
    assert transformed_image.ndim == 2
    return transformed_image


def add_gaussian_noise(x, sigma):
    x += np.random.randn(*x.shape) * sigma
    x = np.clip(x, 0., 1.)
    return x


def _evaluate_ratio(ratio):
    if ratio <= 0.:
        return False
    return np.random.uniform() < ratio


def apply_aug(aug, image):
    return aug(image=image)['image']


class Transform:
    def __init__(self, affine=False, crop=True, size=(64, 64),
                 normalize=True, threshold=80.,
                 sigma=-1., blur_ratio=0., noise_ratio=0., cutout_ratio=0.,
                 grid_distortion_ratio=0., elastic_distortion_ratio=0.,
                 random_brightness_ratio=0.,
                 piece_affine_ratio=0., ssr_ratio=0.):

        self.affine = affine
        self.crop = crop
        self.size = size
        self.normalize = normalize
        self.threshold = threshold / 255.
        self.sigma = sigma / 255.

        self.blur_ratio = blur_ratio
        self.noise_ratio = noise_ratio
        self.cutout_ratio = cutout_ratio
        self.grid_distortion_ratio = grid_distortion_ratio
        self.elastic_distortion_ratio = elastic_distortion_ratio
        self.random_brightness_ratio = random_brightness_ratio
        self.piece_affine_ratio = piece_affine_ratio
        self.ssr_ratio = ssr_ratio

    def __call__(self, example):
        x = example

        # --- Augmentation ---
        if self.affine:
            x = affine_image(x)

        # albumentations...
        x = x.astype(np.float32)
        assert x.ndim == 2
        # 1. blur
        if _evaluate_ratio(self.blur_ratio):
            trigger("blur")
            r = np.random.uniform()
            if r < 0.25:
                x = apply_aug(A.Blur(blur_limit=4, p=1.0), x)
            elif r < 0.5:
                x = apply_aug(A.MedianBlur(blur_limit=5, p=1.0), x)
            elif r < 0.75:
                x = apply_aug(A.GaussianBlur(blur_limit=3, p=1.0), x)
            else:
                x = apply_aug(A.MotionBlur(p=1.0), x)

        # 2. Noise
        if _evaluate_ratio(self.noise_ratio):
            trigger("noise")
            r = np.random.uniform()
            if r < 0.50:
                x = apply_aug(A.GaussNoise(var_limit=5. / 255., p=1.0), x)
            else:
                x = apply_aug(A.MultiplicativeNoise(p=1.0), x)

        # 3. Coarse Dropout
        if _evaluate_ratio(self.cutout_ratio):
            trigger("coarse_dropout")
            x = apply_aug(A.CoarseDropout(
                max_holes=8, max_height=6, max_width=6, p=1.0), x)

        # 4. Distortion
        if _evaluate_ratio(self.grid_distortion_ratio):
            trigger("grid-distortion")
            x = apply_aug(A.GridDistortion(
                num_steps=10, distort_limit=0.2, p=1.0), x)

        if _evaluate_ratio(self.elastic_distortion_ratio):
            trigger("elastic-distortion")
            x = apply_aug(A.ElasticTransform(
                sigma=50, alpha=1, alpha_affine=10, p=1.0), x)

        # 5. Brightness
        if _evaluate_ratio(self.random_brightness_ratio):
            trigger("rand_brightness")
            x = apply_aug(A.RandomBrightnessContrast(p=1.0), x)

        # 6. Affine
        if _evaluate_ratio(self.piece_affine_ratio):
            trigger("albu-affine")
            x = apply_aug(A.IAAPiecewiseAffine(nb_rows=2, nb_cols=2, p=1.0), x)

        if _evaluate_ratio(self.ssr_ratio):
            trigger("albu-ssr")
            x = apply_aug(A.ShiftScaleRotate(
                shift_limit=0.0625,
                scale_limit=0.2,
                rotate_limit=4,
                p=1.0, border_mode=cv2.BORDER_CONSTANT), x)

        # --- Train/Test common preprocessing ---
        if self.crop:
            x = crop_char_image(x, threshold=self.threshold)
        if self.size is not None:
            x = resize(x, size=self.size)
        if self.sigma > 0.:
            x = add_gaussian_noise(x, sigma=self.sigma)

        if self.normalize:
            x = (x.astype(np.float32) - 0.0692) / 0.2051

        if x.ndim == 2:
            x = x[None, :, :]
        x = x.astype(np.float32)
        return x
