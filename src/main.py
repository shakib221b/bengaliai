import pytorch_lightning as pl
import argparse
import os
from pytorch_lightning.callbacks import ModelCheckpoint
from lightning import BengaliAIModule
from models import BengaliModel
from dataset import BengaliDataset
from transformations import Transform


def main(hparams):
    n_grapheme = 168
    n_vowel = 11
    n_consonant = 7
    n_total = n_grapheme + n_vowel + n_consonant
    cb = ModelCheckpoint(
        filepath=f'{hparams.model_dir}{hparams.model_name}.h5')

    predictor = BengaliModel(
        out_channels=n_total,
        model_name=hparams.model_name, pretrained=None
    )

    train_folds = [0, 1, 2, 3]
    image_size = 128
    train_transform = Transform(
        size=(image_size, image_size), threshold=120.,
        sigma=-1., blur_ratio=0.8, noise_ratio=0., cutout_ratio=0.9,
        grid_distortion_ratio=0.6, random_brightness_ratio=0.9,
        piece_affine_ratio=0.4, ssr_ratio=0.9)
    train_dataset = BengaliDataset(train_folds, transform=train_transform)

    valid_folds = [4]
    valid_transform = Transform(
        crop=True, size=(image_size, image_size))
    valid_dataset = BengaliDataset(valid_folds, transform=valid_transform)

    # init model
    model = BengaliAIModule(predictor, train_dataset, valid_dataset)
    print(f'Lightning Module Initiated with backbone {hparams.model_name}')

    trainer = pl.Trainer(
        max_epochs=hparams.epochs,
        checkpoint_callback=cb,
        accumulate_grad_batches=4,
        amp_level='O2', use_amp=False,
        train_percent_check=hparams.check,
        val_percent_check=hparams.check,
        gpus=1
    )

    trainer.fit(model)


if __name__ == '__main__':
    main_arg_parser = argparse.ArgumentParser(
        description="parser for bengaliai competition")
    main_arg_parser.add_argument(
        "--model_name", type=str, default='se_resnext50_32x4d',
        help="state model name"
    )
    main_arg_parser.add_argument(
        "--model_dir", type=str, default='./LightningModels/',
        help="path to folder where trained model will be saved."
    )
    main_arg_parser.add_argument(
        "--epochs", type=int, default=4,
        help="number of epochs to train."
    )
    main_arg_parser.add_argument(
            "--check", type=float, default=1.0,
            help="check")

    # add model specific args
    # parser = BengaliAIModule.add_model_specific_args(
    #     main_arg_parser, os.getcwd())
    args = main_arg_parser.parse_args()

    main(args)
