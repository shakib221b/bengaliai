import torch.nn as nn
import pretrainedmodels
import torch.nn.functional as F


class BengaliModel(nn.Module):
    def __init__(self, model_name="resnet34", out_channels=186, pretrained=True):
        super(BengaliModel, self).__init__()
        self.conv0 = nn.Conv2d(
            1, 3, kernel_size=3, stride=1, padding=1, bias=True)
        if pretrained:
            self.base_model = pretrainedmodels.__dict__[model_name](pretrained="imagenet")
        else:
            self.base_model = pretrainedmodels.__dict__[model_name](pretrained=None)
        self.out = nn.Linear(self.base_model.last_linear.in_features, out_channels)

    def forward(self, x):
        bs, _, _, _ = x.shape
        x = self.conv0(x)
        x = self.base_model.features(x)
        x = F.adaptive_avg_pool2d(x, 1).reshape(bs, -1)
        x = self.out(x)
        return x
