import torch
import pytorch_lightning as pl
import torch.nn.functional as F
from torch.utils.data.dataloader import DataLoader


def accuracy(y, t):
    pred_label = torch.argmax(y, dim=1)
    count = pred_label.shape[0]
    correct = (pred_label == t).sum().type(torch.float32)
    acc = correct / count
    return acc


class BengaliAIModule(pl.LightningModule):

    def __init__(self, predictor, train_dataset, valid_dataset,
                 n_grapheme=168, n_vowel=11, n_consonant=7):

        super(BengaliAIModule, self).__init__()
        self.n_grapheme = n_grapheme
        self.n_vowel = n_vowel
        self.n_consonant = n_consonant
        self.n_total_class = self.n_grapheme + self.n_vowel + self.n_consonant
        self.predictor = predictor
        self.train_dataset = train_dataset
        self.valid_dataset = valid_dataset
        # self.hparams = hparams

    def forward(self, x):
        pred = self.predictor(x)
        assert pred.shape[1] == self.n_total_class
        preds = torch.split(
            pred, [self.n_grapheme, self.n_vowel, self.n_consonant], dim=1)
        return preds

    def training_step(self, batch, batch_idx):
        x, y = batch

        # forward pass
        preds = self.forward(x)

        # calculate loss
        loss_grapheme = F.cross_entropy(preds[0], y[:, 0])
        loss_vowel = F.cross_entropy(preds[1], y[:, 1])
        loss_consonant = F.cross_entropy(preds[2], y[:, 2])

        # full loss
        total_loss = loss_grapheme + loss_vowel + loss_consonant

        tensorboard_logs = {
            'loss_grapheme': loss_grapheme,
            'loss_vowel': loss_vowel,
            'loss_consonant': loss_consonant
        }
        return {
            'loss': total_loss,
            'progress_bar': tensorboard_logs,
            'log': tensorboard_logs}

    def validation_step(self, batch, batch_idx):
        x, y = batch

        # forward pass
        preds = self.forward(x)

        # calculate loss
        loss_grapheme = F.cross_entropy(preds[0], y[:, 0])
        loss_vowel = F.cross_entropy(preds[1], y[:, 1])
        loss_consonant = F.cross_entropy(preds[2], y[:, 2])

        # full loss
        total_loss = loss_grapheme + loss_vowel + loss_consonant

        # calculate accuracy
        acc_grapheme = accuracy(preds[0], y[:, 0])
        acc_vowel = accuracy(preds[1], y[:, 1])
        acc_consonant = accuracy(preds[2], y[:, 2])

        metrics = {
            'val_loss_grapheme': loss_grapheme,
            'val_loss_vowel': loss_vowel,
            'val_loss_consonant': loss_consonant,
            'val_acc_grapheme': acc_grapheme,
            'val_acc_vowel': acc_vowel,
            'val_acc_consonant': acc_consonant
        }
        return {'val_loss': total_loss, 'metrics': metrics}

    def validation_end(self, outputs):
        # OPTIONAL
        val_loss_mean = torch.stack([x['val_loss'] for x in outputs]).mean()

        loss_grapheme_mean = torch.stack(
            [x['metrics']['val_loss_grapheme'] for x in outputs]).mean()
        acc_grapheme_mean = torch.stack(
            [x['metrics']['val_acc_grapheme'] for x in outputs]).mean()

        loss_vowel_mean = torch.stack(
            [x['metrics']['val_loss_vowel'] for x in outputs]).mean()
        acc_vowel_mean = torch.stack(
            [x['metrics']['val_acc_vowel'] for x in outputs]).mean()

        loss_consonant_mean = torch.stack(
            [x['metrics']['val_loss_consonant'] for x in outputs]).mean()
        acc_consonant_mean = torch.stack(
            [x['metrics']['val_acc_consonant'] for x in outputs]).mean()

        tensorboard_logs = {
            'val_loss': val_loss_mean.item(),
            'val_loss_grapheme': loss_grapheme_mean.item(),
            'val_loss_vowel': loss_vowel_mean.item(),
            'val_loss_consonant': loss_consonant_mean.item(),
            'val_acc_grapheme': acc_grapheme_mean.item(),
            'val_acc_vowel': acc_vowel_mean.item(),
            'val_acc_consonant': acc_consonant_mean.item()
        }

        return {
            'progress_bar': tensorboard_logs,
            'log': tensorboard_logs}

    def configure_optimizers(self):
        return torch.optim.Adam(self.parameters())

    @pl.data_loader
    def train_dataloader(self):
        train_dataset = self.train_dataset

        train_loader = DataLoader(
            train_dataset, batch_size=128, shuffle=True)

        return train_loader

    @pl.data_loader
    def val_dataloader(self):
        valid_dataset = self.valid_dataset

        valid_loader = DataLoader(
            valid_dataset, batch_size=128, shuffle=False)

        return valid_loader

    # @staticmethod
    # def add_model_specific_args(parent_parser, root_dir):
    #     model_parser = argparse.ArgumentParser(
    #         parents=[parent_parser],
    #         add_help=False)

    #     model_parser.add_argument(
    #         "--epochs", type=int, default=4,
    #         help="number of training epochs, default is 4")

    #     model_parser.add_argument(
    #         "--batch_size", type=int, default=128,
    #         help="batch size for training, default is 128")

    #     model_parser.add_argument(
    #         "--image_size", type=int, default=64,
    #         help="size of training images, default is 64 X 64")

    #     return model_parser
