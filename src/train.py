import os
import ast
from model_dispatcher import MODEL_DISPATCHER
from dataset import BengaliDataset
from torch.utils.data import DataLoader
import torch


DEVICE = "cuda"
TRAINING_FOLDS_CSV = os.environ.get("TRAINING_FOLDS_CSV")
IMG_HEIGHT = int(os.environ.get("IMG_HEIGHT"))
IMG_WIDTH = int(os.environ.get("IMG_WIDTH"))
EPOCHS = int(os.environ.get("EPOCHS"))

TRAIN_BATCH_SIZE = int(os.environ.get("TRAIN_BATCH_SIZE"))
TEST_BATCH_SIZE = int(os.environ.get("TEST_BATCH_SIZE"))

MODEL_MEAN = ast.literal_eval(os.environ.get("MODEL_MEAN"))
MODEL_STD = ast.literal_eval(os.environ.get("MODEL_STD"))

TRAINING_FOLDS = ast.literal_eval(os.environ.get("TRAIN_BATCH_SIZE"))
VALIDATION_FOLDS = ast.literal_eval(os.environ.get("TEST_BATCH_SIZE"))
BASE_MODEL = os.environ.get("BASE_MODEL")

 
def train():
    return

def evaluate():
    return


def main():
    model = MODEL_DISPATCHER[BASE_MODEL](pretrained=True)
    model.to(DEVICE)

    train_dataset = BengaliDataset(
        folds=TRAINING_FOLDS,
        img_height=IMG_HEIGHT,
        img_width=IMG_WIDTH,
        mean=MODEL_MEAN,
        std=MODEL_STD
    )

    train_loader = DataLoader(
        train_dataset,
        batch_size=TRAIN_BATCH_SIZE,
        num_workers=4,
        shuffle=True
    )

    valid_dataset = BengaliDataset(
        folds=VALIDATION_FOLDS,
        img_height=IMG_HEIGHT,
        img_width=IMG_WIDTH,
        mean=MODEL_MEAN,
        std=MODEL_STD
    )

    valid_loader = DataLoader(
        valid_dataset,
        batch_size=TEST_BATCH_SIZE,
        num_workers=4,
        shuffle=False
    )

    optimizer = torch.optim.Adam(model.parameters(), lr=1e-4)
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(
        optimizer,
        mode="max",
        patience=5,
        factor=0.3
    )

    for epoch in range(EPOCHS):
        train()
        val_score = evaluate()
        scheduler.step(val_score)

if __name__ == "__main__":
    main()