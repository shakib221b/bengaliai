import pandas as pd
import glob
from tqdm import tqdm
import joblib


if __name__ == "__main__":
    train_files = glob.glob("../input/train_*.parquet")
    print(train_files)
    for file in train_files:
        print(f"Reading file: {file}")
        df = pd.read_parquet(file)
        image_ids = df.image_id.values
        df = df.drop("image_id", axis=1)
        image_arrays = df.values
        for j, image_id in tqdm(enumerate(image_ids), total=len(image_ids)):
            joblib.dump(image_arrays[j, :], f"../input/image_pickles/{image_id}.pkl")