import torch
import joblib
import pandas as pd
from torch.utils.data import Dataset


class BengaliDataset(Dataset):
    def __init__(self, folds, transform, img_height=236, img_width=137, mean=0.445, std=0.229):
        df = pd.read_csv("../input/train_folds.csv")
        df = df[["image_id", "grapheme_root", "vowel_diacritic", "consonant_diacritic", "kfold"]]
        df = df[df.kfold.isin(folds)].reset_index(drop=True)
        self.image_ids = df.image_id.values
        self.labels = df[["grapheme_root", "vowel_diacritic", "consonant_diacritic"]].values
        self.transform = transform
        
    def __len__(self):
        return len(self.image_ids)

    def __getitem__(self, index):
        image = joblib.load(f"../input/image_pickles/{self.image_ids[index]}.pkl")
        image = image.reshape(137, 236).astype(float)
        image = (255 - image)/255.
        # image = Image.fromarray(image).convert("RGB")
        # image = self.aug(image=np.array(image))["image"]
        image = self.transform(image)
        return [
            torch.tensor(image, dtype=torch.float),
            torch.tensor(self.labels[index], dtype=torch.long)
        ]


# image_size = 128
# train_transform = Transform(
#         size=(image_size, image_size), threshold=120.,
#         sigma=-1., blur_ratio=0.8, noise_ratio=0., cutout_ratio=0.9,
#         grid_distortion_ratio=0.6, random_brightness_ratio=0.9,
#         piece_affine_ratio=0.4, ssr_ratio=0.9)

# valid_transform = Transform(
#     crop=True, size=(image_size, image_size))

# # img = joblib.load("../input/image_pickles/Train_0.pkl")
# # img = img.reshape(137, 236).astype(float)
# # img = train_transform(img)
# # print(img.shape)

# dataset = BengaliDataset([0, 1],  train_transform, 137, 236, (0.485, 0.456, 0.406), (0.229, 0.224, 0.225))
# print(len(dataset))
# print(dataset[0][1])

# for i in range(9):
#     img = np.squeeze(dataset[0]['image'].numpy().astype(np.uint8)*255)
#     print(img.shape)
#     # img = np.transpose(img, (1, 2, 0))
#     # print(img.shape)
#     img = Image.fromarray(img)
#     img.save(f'{i}.jpg')

