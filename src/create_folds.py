import pandas as pd
from iterstrat.ml_stratifiers import MultilabelStratifiedKFold


if __name__ == '__main__':
    df = pd.read_csv("../input/train.csv")
    df.loc[:, "kfold"] = -1
    # print(df.head())
    df = df.sample(frac=1, random_state=42).reset_index(drop=True)
    # print(df.head())
    X = df.image_id.values
    y = df[["grapheme_root", "vowel_diacritic", "consonant_diacritic"]].values
    # print(y.shape)
    mskf = MultilabelStratifiedKFold(n_splits=5, random_state=42)

    for fold, (train_, val_) in enumerate(mskf.split(X, y)):
        print("Train: ", train_, "Val: ", val_, "val shape: ", len(val_.shape))
        df.loc[val_, "kfold"] = fold

    print(df.kfold.value_counts())
    df.to_csv("../input/train_folds.csv", index=False)


